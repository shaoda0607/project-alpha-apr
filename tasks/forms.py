from django.forms import ModelForm, DateInput
from tasks.models import Task


class CustomDatetimeInput(DateInput):
    input_type = "datetime-local"


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
        widgets = {
            "start_date": CustomDatetimeInput(format="%Y-%m-%dT%H:%M"),
            "due_date": CustomDatetimeInput(format="%Y-%m-%dT%H:%M"),
        }
