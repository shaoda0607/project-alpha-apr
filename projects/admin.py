from django.contrib import admin
from projects.models import Project


# Register your models here.
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    model = Project
    list_display = [
        "name",
        "description",
        "owner",
    ]
